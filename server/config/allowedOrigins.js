const allowedOrigins = [
  'http://localhost:5000',
  'http://localhost:3000',
  'https://realist-app.onrender.com',
];

export default allowedOrigins;
