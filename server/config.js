import SES from 'aws-sdk/clients/ses.js';
import S3 from 'aws-sdk/clients/s3.js';
import NodeGeocoder from 'node-geocoder';
import dotenv from 'dotenv';
dotenv.config();

export const DATABASE = process.env.DATABASE;

export const JWT_SECRET = process.env.JWT_SECRET;

export const JWT_EXPIRES_IN = process.env.JWT_EXPIRES_IN;

export const CLIENT_URL = process.env.CLIENT_URL;

export const AWS_ACCESS_KEY_ID = process.env.AWS_ACCESS_KEY_ID;

export const AWS_SECRET_ACCESS_KEY = process.env.AWS_SECRET_ACCESS_KEY;

export const EMAIL_FROM = '"Marco Narca" <marco@weredefineimpact.com>';

export const REPLY_TO = 'marco@weredefineimpact.com';

const awsConfig = {
  accessKeyId: AWS_ACCESS_KEY_ID,
  secretAccessKey: AWS_SECRET_ACCESS_KEY,
  region: process.env.REGION,
  apiVersion: process.env.API_VERSION,
};

export const AWSSES = new SES(awsConfig);
export const AWS3 = new S3(awsConfig);

const options = {
  provider: 'google',
  apiKey: process.env.GOOGLE_API_KEY,
  formatter: null,
};

export const GOOGLE_GEOCODER = NodeGeocoder(options);
