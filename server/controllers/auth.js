import {
  AWSSES,
  CLIENT_URL,
  EMAIL_FROM,
  JWT_EXPIRES_IN,
  JWT_SECRET,
  REPLY_TO,
} from '../config.js';
import jwt from 'jsonwebtoken';
import { emailTemplate } from '../helpers/email.js';
import { hashPassword, comparePassword } from '../helpers/auth.js';
import User from '../models/user.js';
import Ad from '../models/ad.js';
import { nanoid } from 'nanoid';
import validator from 'email-validator';

const tokenAndUserResponse = (req, res, user) => {
  const token = jwt.sign({ _id: user._id }, JWT_SECRET, {
    expiresIn: JWT_EXPIRES_IN,
  });

  const refreshToken = jwt.sign({ _id: user._id }, JWT_SECRET, {
    expiresIn: '2d',
  });

  user.password = undefined;
  user.resetCode = undefined;

  return res.json({
    token,
    refreshToken,
    user,
  });
};

export const welcome = (req, res) => {
  res.json({
    data: 'hello from nodejs api',
  });
};

export const preRegister = async (req, res) => {
  try {
    const { email, password } = req.body;

    if (!validator.validate(email)) {
      return res.json({ error: 'A valid email is required' });
    }

    if (!password) {
      return res.json({ error: 'Password is required' });
    }

    if (password && password?.length < 6) {
      return res.json({ error: 'Password should be at least 6 characters' });
    }

    const user = await User.findOne({ email });
    if (user) {
      return res.json({ error: 'Email is Taken' });
    }

    const token = jwt.sign({ email, password }, JWT_SECRET, {
      expiresIn: JWT_EXPIRES_IN,
    });

    AWSSES.sendEmail(
      emailTemplate(
        email,
        `
        <p>Please Click the link below to active the account</p>
        <a href="${CLIENT_URL}/auth/account-activate/${token}">Activate My Account</a>
      `,
        REPLY_TO,
        'Activate your account'
      ),
      (err, data) => {
        if (err) {
          console.log(err);
          return res.json({ ok: false });
        } else {
          console.log(data);
          return res.json({ ok: true });
        }
      }
    );
  } catch (error) {
    console.log(error);
    return res.json({ error: 'Something went wrong. Try again.' });
  }
};

export const register = async (req, res) => {
  try {
    const { email, password } = jwt.verify(req.body.token, JWT_SECRET);

    const userExist = await User.findOne({ email });
    if (userExist) {
      return res.json({ error: 'Email is Taken' });
    }

    const hashedPassword = await hashPassword(password);
    const user = await new User({
      username: nanoid(6),
      email,
      password: hashedPassword,
    }).save();

    tokenAndUserResponse(req, res, user);
  } catch (error) {
    console.log(error);
    return res.json({ error: 'Something went wrong. Try again.' });
  }
};

export const login = async (req, res) => {
  try {
    const { email, password } = req.body;

    const user = await User.findOne({ email });

    if (!user) {
      return res.json({ error: 'No User found. Please register.' });
    }

    const match = await comparePassword(password, user.password);

    if (!match) {
      return res.json({ error: 'Wrong Password' });
    }

    tokenAndUserResponse(req, res, user);
  } catch (error) {
    console.log(error);
    return res.json({ error: 'Something went wrong. Try again.' });
  }
};

export const forgotPassword = async (req, res) => {
  try {
    const { email } = req.body;
    const user = await User.findOne({ email });
    if (!user) {
      return res.json({ error: 'Could not find user with that email' });
    } else {
      const resetCode = nanoid();
      user.resetCode = resetCode;
      user.save();

      const token = jwt.sign({ resetCode }, JWT_SECRET, {
        expiresIn: '1h',
      });

      AWSSES.sendEmail(
        emailTemplate(
          email,
          `
          <p>Please click the link below to access your account.</p>
          <a href="${CLIENT_URL}/auth/access-account/${token}">Access my account</a>
        `,
          REPLY_TO,
          'Access your account'
        ),
        (err, data) => {
          if (err) {
            console.log(err);
            return res.json({ ok: false });
          } else {
            console.log(data);
            return res.json({ ok: true });
          }
        }
      );
    }
  } catch (error) {
    console.log(error);
    return res.json({ error: 'Something went wrong. Try again.' });
  }
};

export const accessAccount = async (req, res) => {
  try {
    const { resetCode } = jwt.verify(req.body.resetCode, JWT_SECRET);

    const user = await User.findOneAndUpdate({ resetCode }, { resetCode: '' });

    tokenAndUserResponse(req, res, user);
  } catch (error) {
    console.log(error);
    return res.json({ error: 'Something went wrong. Try again.' });
  }
};

export const refreshToken = async (req, res) => {
  try {
    const { _id } = jwt.verify(req.headers.refresh_token, JWT_SECRET);

    const user = await User.findById(_id);

    tokenAndUserResponse(req, res, user);
  } catch (error) {
    console.log(error);
    return res.status(403).json({ error: 'Unauthorized' });
  }
};

export const currentUser = async (req, res) => {
  try {
    const user = await User.findById(req.user._id);
    user.password = undefined;
    user.resetCode = undefined;
    res.json(user);
  } catch (error) {
    console.log(error);
    return res.status(403).json({ error: 'Unauthorized' });
  }
};

export const publicProfile = async (req, res) => {
  try {
    const user = await User.findOne({ username: req.params.username });

    user.password = undefined;
    user.resetCode = undefined;
    res.json(user);
  } catch (error) {
    console.log(error);
    return res.status(404).json({ error: 'User Not Found' });
  }
};

export const updatePassword = async (req, res) => {
  try {
    const { password } = req.body;
    if (!password) {
      return res.json({ error: 'Password is required' });
    }

    if (password && password?.length < 6) {
      return res.json({ error: 'Password should be min 6 characters' });
    }

    const user = await User.findByIdAndUpdate(req.user._id, {
      password: await hashPassword(password),
    });

    res.json({ ok: true });
  } catch (error) {
    console.log(error);
    return res.status(404).json({ error: 'User Not Found' });
  }
};

export const updateProfile = async (req, res) => {
  try {
    const user = await User.findByIdAndUpdate(req.user._id, req.body, {
      new: true,
    });

    user.password = undefined;
    user.resetCode = undefined;

    res.json(user);
  } catch (error) {
    if (error.codeName === 'DuplicateKey') {
      return res.json({ error: 'Username or email is already taken' });
    } else {
      return res.status(404).json({ error: 'User Not Found' });
    }
  }
};

export const agents = async (req, res) => {
  try {
    const agents = await User.find({ role: 'Seller' }).select(
      '-password -role -wishlist -enquiredProperties -photo.key -photo.Key -photo.Bucket'
    );

    res.json(agents);
  } catch (error) {
    console.log(error);
    res.json({ error: 'Something went wrong' });
  }
};

export const agentAdCount = async (req, res) => {
  try {
    const ads = await Ad.find({ postedBy: req.params._id }).select('_id');

    res.json(ads);
  } catch (error) {
    console.log(error);
    res.json({ error: 'Something went wrong' });
  }
};

export const agent = async (req, res) => {
  try {
    const user = await User.findOne({ username: req.params.username }).select(
      '-password -role -wishlist -enquiredProperties -photo.key -photo.Key -photo.Bucket'
    );

    const ads = await Ad.find({ postedBy: user._id }).select(
      '-photos.key -photos.Key -photos.ETag -photos.Bucket -location -googleMap'
    );

    res.json({ user, ads });
  } catch (error) {
    console.log(error);
    res.json({ error: 'Something went wrong' });
  }
};
