import { AWS3, AWSSES, GOOGLE_GEOCODER, CLIENT_URL } from '../config.js';
import { nanoid } from 'nanoid';
import slugify from 'slugify';
import Ad from '../models/ad.js';
import User from '../models/user.js';
import { emailTemplate } from '../helpers/email.js';

export const uploadImage = async (req, res) => {
  try {
    const { image } = req.body;
    const base64Image = new Buffer.from(
      image.replace(/^data:image\/\w+;base64,/, ''),
      'base64'
    );

    const type = image.split(';')[0].split('/')[1];

    const params = {
      Bucket: 'realist-app',
      Key: `${nanoid()}.${type}`,
      Body: base64Image,
      ACL: 'public-read',
      ContentEncoding: 'base64',
      ContentType: `image/${type}`,
    };

    AWS3.upload(params, (err, data) => {
      if (err) {
        console.log(err);
        res.sendStatus(400);
      } else {
        res.send(data);
      }
    });
  } catch (error) {
    console.log(error);
    res.json({ error: 'Upload failed. Try again.' });
  }
};

export const removeImage = async (req, res) => {
  try {
    const { Key, Bucket } = req.body;
    AWS3.deleteObject({ Bucket, Key }, (err, data) => {
      if (err) {
        console.log(err);
        res.sendStatus(400);
      } else {
        res.send({ ok: true });
      }
    });
  } catch (error) {
    console.log(error);
  }
};

export const create = async (req, res) => {
  try {
    const { photos, description, title, address, price, type, landSize } =
      req.body;

    if (!photos?.length) {
      return res.json({ error: 'Photos are required' });
    }

    if (!price) {
      return res.json({ error: 'Price is required' });
    }

    if (!type) {
      return res.json({ error: 'Type property is required' });
    }

    if (!address) {
      return res.json({ error: 'Address is required' });
    }

    if (!description) {
      return res.json({ error: 'Description is required' });
    }
    if (!title) {
      return res.json({ error: 'Title is required' });
    }
    if (!landSize) {
      return res.json({ error: 'landSize is required' });
    }

    const geo = await GOOGLE_GEOCODER.geocode(address);
    const ad = await new Ad({
      ...req.body,
      landsize: landSize,
      postedBy: req.user._id,
      location: {
        type: 'Point',
        coordinates: [geo?.[0]?.longitude, geo?.[0]?.latitude],
      },
      googleMap: geo,
      slug: slugify(`${title}-${address}-${price}-${nanoid(6)}`),
    }).save();

    const user = await User.findByIdAndUpdate(
      req.user._id,
      {
        $addToSet: { role: 'Seller' },
      },
      {
        new: true,
      }
    );

    user.password = undefined;
    user.resetCode = undefined;
    res.json({
      ad,
      user,
    });
  } catch (error) {
    res.json({ error: 'Something went wrong. Try again.' });
    console.log(error);
  }
};

export const ads = async (req, res) => {
  try {
    const adsForSell = await Ad.find({ action: 'Sell' })
      .select('-googleMap -location -photo.Key -photo.key -photo.ETag')
      .sort({ createdAt: -1 })
      .limit(12);

    const adsForRent = await Ad.find({ action: 'Rent' })
      .select('-googleMap -location -photo.Key -photo.key -photo.ETag')
      .sort({ createdAt: -1 })
      .limit(12);

    res.json({ adsForRent, adsForSell });
  } catch (error) {
    res.json({ error: 'Something went wrong. Try again.' });
    console.log(error);
  }
};

export const read = async (req, res) => {
  try {
    const ad = await Ad.findOne({ slug: req.params.slug }).populate(
      'postedBy',
      'name username email phone company photo.Location'
    );

    const related = await Ad.find({
      _id: { $ne: ad._id },
      action: ad.action,
      type: ad.type,
      address: {
        $regex: ad.googleMap[0]?.administrativeLevels.level2long || '',
        $options: 'i',
      },
    })
      .limit(3)
      .select('-photos.Key -photos.key -photos.ETag -photos.Bucket -googleMap');

    res.json({ ad, related });
  } catch (error) {
    res.json({ error: 'Something went wrong. Try again.' });
    console.log(error);
  }
};

export const addToWishList = async (req, res) => {
  try {
    const user = await User.findByIdAndUpdate(
      req.user._id,
      {
        $addToSet: { wishlist: req.body.adId },
      },
      {
        new: true,
      }
    );

    const { password, resetCode, ...rest } = user._doc;

    res.json(rest);
  } catch (error) {
    res.json({ error: 'Something went wrong. Try again.' });
    console.log(error);
  }
};

export const removeFromWishList = async (req, res) => {
  try {
    const user = await User.findByIdAndUpdate(
      req.user._id,
      {
        $pull: { wishlist: req.params.adId },
      },
      {
        new: true,
      }
    );

    const { password, resetCode, ...rest } = user._doc;
    res.json(rest);
  } catch (error) {
    res.json({ error: 'Something went wrong. Try again.' });
    console.log(error);
  }
};

export const contactSeller = async (req, res) => {
  try {
    const { name, email, message, phone, adId } = req.body;
    const ad = await Ad.findById(adId).populate('postedBy', 'email');

    const user = await User.findByIdAndUpdate(req.user._id, {
      $addToSet: { enquiredProperties: adId },
    });

    if (!user) {
      return res.json({ error: 'Could not find user with that email' });
    } else {
      AWSSES.sendEmail(
        emailTemplate(
          ad.postedBy.email,
          `
        <p>You have received a new customer Inquiry</p>
        <p>Name: ${name}</p>
        <p>Email: ${email}</p>
        <p>Phone: ${phone}</p>
        <h4>Message: ${message}</h4>
        <a href="${CLIENT_URL}/ad/${ad.slug}">${ad.type} in ${ad.address} for ${ad.action} ${ad.price}</a>
      `,
          email,
          'New Inquiry Received'
        ),
        (err, data) => {
          if (err) {
            console.log(err);
            return res.json({ ok: false });
          } else {
            console.log(data);
            return res.json({ ok: true });
          }
        }
      );
    }
  } catch (error) {
    console.log(error);
    res.json({ error: 'Something went wrong. Try again.' });
  }
};

export const userAds = async (req, res) => {
  try {
    const perPage = 2;
    const page = req.params.page ? req.params.page : 1;

    const total = await Ad.find({ postedBy: req.user._id });
    const ads = await Ad.find({ postedBy: req.user._id })
      .select(
        '-photos.Key -photos.key -photos.ETag -photos.Bucket -location -googleMap'
      )
      .populate('postedBy', 'name email username phone company')
      .skip((page - 1) * perPage)
      .limit(perPage)
      .sort({ createdAt: -1 });

    res.json({ ads, total: total.length });
  } catch (error) {
    console.log(error);
    res.json({ error: 'Something went wrong. Try again.' });
  }
};

export const update = async (req, res) => {
  try {
    const { photos, price, type, address, description } = req.body;

    const ad = await Ad.findById(req.params._id);
    const owner = req.user._id === ad?.postedBy.toString();

    console.log(req.user._id);
    console.log(ad?.postedBy.toString());
    if (!owner) {
      return res.json({ error: 'Permission Denied' });
    } else {
      if (!photos.length) {
        return res.json({ error: 'Photos are required' });
      }

      if (!price) {
        return res.json({ error: 'Price are required' });
      }

      if (!type) {
        return res.json({ error: 'Type are required' });
      }

      if (!address) {
        return res.json({ error: 'Address are required' });
      }

      if (!description) {
        return res.json({ error: 'Description are required' });
      }

      const geo = await GOOGLE_GEOCODER.geocode(address);

      ad.photos = photos;
      ad.price = price;
      ad.type = type;
      ad.address = address;
      ad.description = description;
      ad.location.coodinates = [geo?.[0]?.longitude, geo?.[0]?.latitude];

      await ad.save();
      res.json({ ok: true });
    }
  } catch (error) {
    console.log(error);
    res.json({ error: 'Something went wrong. Try again.' });
  }
};

export const inquiryProperties = async (req, res) => {
  try {
    const user = await User.findById(req.user._id);
    const ads = await Ad.find({ _id: user.enquiredProperties }).sort({
      createdAt: -1,
    });

    res.json(ads);
  } catch (error) {
    console.log(error);
  }
};

export const wishlist = async (req, res) => {
  try {
    const user = await User.findById(req.user._id);
    const ads = await Ad.find({ _id: user.wishlist }).sort({
      createdAt: -1,
    });

    res.json(ads);
  } catch (error) {
    console.log(error);
  }
};

export const remove = async (req, res) => {
  try {
    const ad = await Ad.findById(req.params._id);
    const owner = req.user._id === ad?.postedBy.toString();

    if (!owner) {
      return res.json({ error: 'Permission Denied' });
    } else {
      await Ad.findByIdAndRemove(ad._id);
      res.json({ ok: true });
    }
  } catch (error) {
    console.log(error);
  }
};

export const adsForSell = async (req, res) => {
  try {
    const ads = await Ad.find({ action: 'Sell' })
      .select('-googleMap -location -photo.Key -photo.key -photo.ETag')
      .sort({ createdAt: -1 })
      .limit(12);

    res.json(ads);
  } catch (error) {
    res.json({ error: 'Something went wrong. Try again.' });
    console.log(error);
  }
};

export const adsForRent = async (req, res) => {
  try {
    const ads = await Ad.find({ action: 'Rent' })
      .select('-googleMap -location -photo.Key -photo.key -photo.ETag')
      .sort({ createdAt: -1 })
      .limit(12);

    res.json(ads);
  } catch (error) {
    res.json({ error: 'Something went wrong. Try again.' });
    console.log(error);
  }
};

export const search = async (req, res) => {
  try {
    const { action, address, type, priceRange } = req.query;

    const geo = await GOOGLE_GEOCODER.geocode(address);
    const ads = await Ad.find({
      action: action === 'Buy' ? 'Sell' : 'Rent',
      type,
      price: {
        $gte: parseInt(priceRange[0]),
        $lte: parseInt(priceRange[1]),
      },
      location: {
        $near: {
          $maxDistance: 50000, // 1000m = 1km
          $geometry: {
            type: 'Point',
            coordinates: [geo?.[0]?.longitude, geo?.[0]?.latitude],
          },
        },
      },
    })
      .limit(24)
      .sort({ createdAt: -1 })
      .select(
        '-photos.key -photos.Key -photos.ETag -photos.Bucket -location -googleMap'
      );

    res.json(ads);
  } catch (error) {
    res.json({ error: 'Something went wrong. Try again.' });
    console.log(error);
  }
};
