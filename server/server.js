import dotenv from 'dotenv';
import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import mongoose from 'mongoose';
import { DATABASE } from './config.js';
import authRoutes from './routes/auth.js';
import adRoutes from './routes/ad.js';
import path, { dirname } from 'path';
import { fileURLToPath } from 'url';
import corsOptions from './config/corsOptions.js';

dotenv.config();
const app = express();
const __dirname = dirname(fileURLToPath(import.meta.url));

// middlewares
app.use(express.json({ limit: '10mb' }));
app.use(morgan('dev'));

app.use(cors(corsOptions));

app.use(express.static(path.join(__dirname, '..', 'client', 'build')));

// routes middleware
app.use('/api', authRoutes);
app.use('/api', adRoutes);
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, '..', 'client', 'build', 'index.html'));
});

const PORT = process.env.PORT || 5000;

// db
mongoose.set('strictQuery', false);
mongoose
  .connect(DATABASE)
  .then(() => {
    console.log(`MongoDB is connected...`);
    app.listen(PORT, () => {
      console.log(`Server is running on PORT: ${PORT}...`);
    });
  })
  .catch((error) => {
    console.log(error);
  });
