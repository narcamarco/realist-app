import express from 'express';
import {
  preRegister,
  welcome,
  register,
  login,
  forgotPassword,
  accessAccount,
  refreshToken,
  currentUser,
  publicProfile,
  updatePassword,
  updateProfile,
  agents,
  agentAdCount,
  agent,
} from '../controllers/auth.js';
import { requireSignIn } from '../middlewares/auth.js';

const router = express.Router();

router.get('/', requireSignIn, welcome);
router.post('/pre-register', preRegister);
router.post('/register', register);
router.post('/login', login);
router.post('/forgot-password', forgotPassword);
router.post('/access-account', accessAccount);
router.get('/refresh-token', refreshToken);
router.get('/current-user', requireSignIn, currentUser);
router.get('/profile/:username', publicProfile);
router.put('/update-password', requireSignIn, updatePassword);
router.put('/update-profile', requireSignIn, updateProfile);
router.get('/agents', agents);
router.get('/agent-ad-count/:_id', agentAdCount);
router.get('/agent/:username', agent);

export default router;
