import express from 'express';
import {
  uploadImage,
  removeImage,
  create,
  ads,
  read,
  addToWishList,
  removeFromWishList,
  contactSeller,
  userAds,
  update,
  inquiryProperties,
  wishlist,
  remove,
  adsForSell,
  adsForRent,
  search,
} from '../controllers/ad.js';
import { requireSignIn } from '../middlewares/auth.js';

const router = express.Router();

router.post('/upload-image', requireSignIn, uploadImage);
router.post('/remove-image', requireSignIn, removeImage);
router.post('/ad', requireSignIn, create);
router.get('/ads', ads);
router.get('/estate/:slug', read);

router.post('/wishlist', requireSignIn, addToWishList);
router.delete('/wishlist/:adId', requireSignIn, removeFromWishList);

router.post('/contact-seller', requireSignIn, contactSeller);
router.get('/user-ads/:page', requireSignIn, userAds);
router.put('/estate/:_id', requireSignIn, update);
router.get('/inquiry-properties', requireSignIn, inquiryProperties);
router.get('/wishlist', requireSignIn, wishlist);
router.delete('/estate/:_id', requireSignIn, remove);

router.get('/ads-for-sell', adsForSell);
router.get('/ads-for-rent', adsForRent);

router.get('/search', search);
export default router;
