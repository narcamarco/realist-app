const sellPrices = [
  {
    _id: 0,
    name: 'All price',
    array: [0, 1000000],
  },
  {
    _id: 1,
    name: '$0 to $200000',
    array: [0, 200000],
  },
  {
    _id: 2,
    name: '$200000 to $500000',
    array: [200000, 500000],
  },
  {
    _id: 3,
    name: '$500000 to $1000000',
    array: [500000, 1000000],
  },
];

const rentPrices = [
  {
    _id: 0,
    name: 'All price',
    array: [0, 200000],
  },
  {
    _id: 1,
    name: '$0 to $40000',
    array: [0, 40000],
  },
  {
    _id: 2,
    name: '$40000 to $100000',
    array: [40000, 100000],
  },
  {
    _id: 3,
    name: '$100000 to $200000',
    array: [100000, 200000],
  },
];

export { sellPrices, rentPrices };
