import React from 'react';
import GoogleMapReact from 'google-map-react';
import { GOOGLE_MAPS_KEY } from '../../config';
import { IoLocationSharp } from 'react-icons/io5';

const MapCard = ({ ad }) => {
  const defaultProps = {
    center: {
      lat: ad?.location?.coordinates[1],
      lng: ad?.location?.coordinates[0],
    },
    zoom: 11,
  };

  return (
    <div style={{ width: '100%', height: '350px' }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: GOOGLE_MAPS_KEY }}
        center={defaultProps.center}
        zoom={defaultProps.zoom}
      >
        <div
          lat={ad?.location?.coordinates[1]}
          lng={ad?.location?.coordinates[0]}
        >
          <span className="lead">
            <IoLocationSharp />
          </span>
        </div>
      </GoogleMapReact>
    </div>
  );
};

export default MapCard;
