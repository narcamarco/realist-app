import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

const RedirectRoute = () => {
  const [count, setCount] = useState(5);
  const navigate = useNavigate();

  useEffect(() => {
    const interval = setInterval(() => {
      setCount((currentCount) => {
        return --currentCount;
      });
    }, 1000);

    if (count === 1) {
      navigate('/');
    }

    return () => clearInterval(interval);
  }, [count]);

  return (
    <div
      className="d-flex justify-content-center align-items-center vh-100"
      style={{ marginTop: '7%' }}
    >
      <h2>Please Login Redirecting...</h2>
    </div>
  );
};

export default RedirectRoute;
