import React from 'react';
import { useAuth } from '../../context/auth';
import { FcLike, FcLikePlaceholder } from 'react-icons/fc';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import toast from 'react-hot-toast';

const LikeUnlike = ({ ad }) => {
  const [auth, setAuth] = useAuth();
  const navigate = useNavigate();

  const handleLike = async () => {
    try {
      if (auth.user === null) {
        navigate('/login', {
          state: `/ad/${ad.slug}`,
        });
        return;
      }

      const { data } = await axios.post('/wishlist', { adId: ad._id });
      setAuth({ ...auth, user: data });

      const fromLS = JSON.parse(localStorage.getItem('auth'));

      fromLS.user = data;
      localStorage.setItem('auth', JSON.stringify(fromLS));
      toast.success('Added to the wishlist');
    } catch (error) {
      console.log(error);
    }
  };

  const handleUnlike = async () => {
    try {
      if (auth.user === null) {
        navigate('/login', {
          state: `/ad/${ad.slug}`,
        });
        return;
      }

      const { data } = await axios.delete(`/wishlist/${ad._id}`);

      setAuth({ ...auth, user: data });

      const fromLS = JSON.parse(localStorage.getItem('auth'));

      fromLS.user = data;
      localStorage.setItem('auth', JSON.stringify(fromLS));
      toast.success('remove to the wishlist');
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      {auth.user?.wishlist?.includes(ad?._id) ? (
        <span>
          <FcLike className="h2 mt-3 pointer" onClick={handleUnlike} />
        </span>
      ) : (
        <span>
          <FcLikePlaceholder className="h2 mt-3 pointer" onClick={handleLike} />
        </span>
      )}
    </>
  );
};

export default LikeUnlike;
