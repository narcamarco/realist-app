import React, { useState, useCallback } from 'react';
import Gallery from 'react-photo-gallery';
import Carousel, { Modal, ModalGateway } from 'react-images';

const ImageGallery = ({ photos }) => {
  const [current, setCurrent] = useState(0);
  const [isOpen, setIsOpen] = useState(false);

  const openLightBox = useCallback((event, { photo, index }) => {
    setCurrent(index);
    setIsOpen(true);
  }, []);

  const closeLightBox = () => {
    setCurrent(0);
    setIsOpen(false);
  };

  return (
    <>
      <Gallery photos={photos} onClick={openLightBox} />
      <ModalGateway>
        {isOpen ? (
          <Modal onClose={closeLightBox}>
            <Carousel
              currentIndex={current}
              views={photos.map((x) => {
                return {
                  ...x,
                  srcset: x.srcSet,
                  caption: x.title,
                };
              })}
            />
          </Modal>
        ) : null}
      </ModalGateway>
    </>
  );
};

export default ImageGallery;
