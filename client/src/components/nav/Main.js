import React from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import { useAuth } from '../../context/auth';

const Main = () => {
  const [auth, setAuth] = useAuth();
  const navigate = useNavigate();

  const logout = async () => {
    setAuth({ user: null, token: '', refreshToken: '' });
    localStorage.removeItem('auth');
    navigate('/login');
  };

  const loggedIn =
    auth.user !== null && auth.token !== '' && auth.refreshToken !== '';

  const handlePostAdClick = () => {
    if (loggedIn) {
      navigate('/ad/create');
    } else {
      navigate('/login');
    }
  };

  return (
    <nav className="nav d-flex justify-content-between lead">
      <NavLink to="/" className="nav-link">
        Home
      </NavLink>

      <NavLink to="/search" className="nav-link">
        Search
      </NavLink>

      <NavLink to="/buy" className="nav-link">
        Buy
      </NavLink>

      <NavLink to="/rent" className="nav-link">
        Rent
      </NavLink>

      <NavLink to="/agents" className="nav-link">
        Agents
      </NavLink>

      <a className="nav-link pointer" onClick={handlePostAdClick}>
        Post Ad
      </a>

      {!loggedIn ? (
        <>
          <NavLink to="/login" className="nav-link">
            Login
          </NavLink>

          <NavLink to="/register" className="nav-link">
            Register
          </NavLink>
        </>
      ) : (
        ''
      )}

      {loggedIn ? (
        <div className="dropdown">
          <li>
            <a
              className="nav-link dropdown-toggle pointer"
              data-bs-toggle="dropdown"
            >
              {auth?.user?.name ? auth.user.name : auth.user.username}
            </a>
            <ul className="dropdown-menu">
              <li>
                <NavLink to="/dashboard" className="nav-link">
                  Dashboard
                </NavLink>
              </li>
              <li>
                <a className="nav-link" onClick={logout}>
                  Logout
                </a>
              </li>
            </ul>
          </li>
        </div>
      ) : (
        ''
      )}
    </nav>
  );
};

export default Main;
