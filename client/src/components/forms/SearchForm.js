import React from 'react';
import { useSearch } from '../../context/search';
import GooglePlacesAutocomplete from 'react-google-places-autocomplete';
import { GOOGLE_PLACES_KEY } from '../../config';
import { rentPrices, sellPrices } from '../../helpers/priceList';
import { FcApproval } from 'react-icons/fc';
import queryString from 'query-string';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';


const SearchForm = () => {
  const [search, setSearch] = useSearch();

  const navigate = useNavigate();

  const handleSearch = async () => {
    setSearch({ ...search, loading: true });
    try {
      const { results, page, price, ...rest } = search;
      const query = queryString.stringify(rest);
      console.log(query);

      const { data } = await axios.get(`/search?${query}`);

      if (search?.page !== '/search') {
        setSearch((prev) => {
          return {
            ...prev,
            results: data,
            loading: false,
          };
        });

        navigate('/search');
      } else {
        setSearch((prev) => {
          return {
            ...prev,
            results: data,
            loading: false,
            page: window.location.pathname,
          };
        });
      }
    } catch (error) {
      console.log(error);
      setSearch({ ...search, loading: false });
    }
  };

  return (
    <>
      <div className="container m-5">
        <div className="row">
          <div className="col-lg-12 form-control">
            <GooglePlacesAutocomplete
              apiKey={GOOGLE_PLACES_KEY}
              apiOptions="au"
              selectProps={{
                defaultInputValue: search?.address,
                placeholder: 'Search for address...',
                onChange: ({ value }) => {
                  setSearch({ ...search, address: value.description });
                },
              }}
            />
          </div>
        </div>
        <div className="d-flex justify-content-center mt-3">
          <button
            className="btn btn-primary col-lg-2 square"
            onClick={() => setSearch({ ...search, action: 'Buy', price: '' })}
          >
            {search.action === 'Buy' ? (
              <span className="d-flex justify-content-center align-items-center">
                <FcApproval className="mr-1" /> Buy
              </span>
            ) : (
              'Buy'
            )}
          </button>
          <button
            className="btn btn-primary col-lg-2 square"
            onClick={() => setSearch({ ...search, action: 'Rent', price: '' })}
          >
            {search.action === 'Rent' ? (
              <span className="d-flex justify-content-center align-items-center">
                <FcApproval className="mr-1" /> Rent
              </span>
            ) : (
              'Rent'
            )}
          </button>
          <button
            className="btn btn-primary col-lg-2 square"
            onClick={() => setSearch({ ...search, type: 'House', price: '' })}
          >
            {search.type === 'House' ? (
              <span className="d-flex justify-content-center align-items-center">
                <FcApproval className="mr-1" /> House
              </span>
            ) : (
              'House'
            )}
          </button>
          <button
            className="btn btn-primary col-lg-2 square"
            onClick={() => setSearch({ ...search, type: 'Land', price: '' })}
          >
            {search.type === 'Land' ? (
              <span className="d-flex justify-content-center align-items-center">
                <FcApproval className="mr-1" /> Land
              </span>
            ) : (
              'Land'
            )}
          </button>

          <div className="dropdown">
            <button
              className="btn btn-primary dropdown-toggle"
              type="button"
              data-bs-toggle="dropdown"
              aria-expanded="false"
            >
              &nbsp; {search?.price ? search.price : 'Price Range'}
            </button>
            <ul className="dropdown-menu">
              {search.action === 'Buy' ? (
                <>
                  {sellPrices?.map((p) => (
                    <li key={p._id}>
                      <a
                        className="dropdown-item"
                        onClick={() =>
                          setSearch({
                            ...search,
                            price: p.name,
                            priceRange: p.array,
                          })
                        }
                      >
                        {p.name}
                      </a>
                    </li>
                  ))}
                </>
              ) : (
                <>
                  {rentPrices?.map((p) => (
                    <li key={p._id}>
                      <a
                        className="dropdown-item"
                        onClick={() =>
                          setSearch({
                            ...search,
                            price: p.name,
                            priceRange: p.array,
                          })
                        }
                      >
                        {p.name}
                      </a>
                    </li>
                  ))}
                </>
              )}
            </ul>
          </div>

          <button
            onClick={handleSearch}
            className="btn btn-danger col-lg-2 square"
          >
            Search
          </button>
        </div>
      </div>
    </>
  );
};

export default SearchForm;
