import React, { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useAuth } from '../../context/auth';
import toast from 'react-hot-toast';
import axios from 'axios';

const ContactSeller = ({ ad }) => {
  const [auth, setAuth] = useAuth();

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');
  const [phone, setPhone] = useState('');
  const [loading, setLoading] = useState(false);

  const navigate = useNavigate();

  const loggedIn = auth.user !== null && auth.token !== '';

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      const { data } = await axios.post('/contact-seller', {
        name,
        email,
        message,
        phone,
        adId: ad._id,
      });

      if (data.error) {
        toast.error(data?.error);
        setLoading(false);
        setMessage('');
      } else {
        toast.success('Your enquiry has been emailed to the seller');
        setLoading(false);
        setMessage('');
      }
    } catch (error) {
      console.log(error);
      toast.error('Something went wrong. Please try again');

      setLoading(false);
    }
  };

  useEffect(() => {
    if (loggedIn) {
      setName(auth.user?.name);
      setEmail(auth.user?.email);
      setPhone(auth.user?.phone);
    }
  }, [loggedIn]);

  return (
    <div className="row">
      <div className="col-lg-8 offset-lg-2">
        <h3>
          Contact{' '}
          {ad?.postedBy?.name ? ad?.postedBy?.name : ad?.postedBy?.username}
        </h3>

        <form onSubmit={handleSubmit}>
          <textarea
            name="message"
            className="form-control mb-3"
            placeholder="Write your message"
            value={message}
            onChange={(e) => setMessage(e.target.value)}
            disabled={!loggedIn}
          ></textarea>

          <input
            type="text"
            className="form-control mb-3"
            placeholder="Enter your name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            disabled={!loggedIn}
          />
          <input
            type="text"
            className="form-control mb-3"
            value={email}
            placeholder="Enter your Email"
            onChange={(e) => setEmail(e.target.value)}
            disabled={!loggedIn}
          />

          <input
            type="text"
            className="form-control mb-3"
            placeholder="Enter your phone"
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
            disabled={!loggedIn}
          />

          <button
            className="btn btn-primary mt-4 mb-5"
            disabled={!name || !email || loading}
            type="submit"
          >
            {loggedIn
              ? loading
                ? 'Please Wait'
                : 'Send Inquiry'
              : 'Login to Send Inquiry'}
          </button>
        </form>
      </div>
    </div>
  );
};

export default ContactSeller;
