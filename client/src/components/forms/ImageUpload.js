import React from 'react';
import Resizer from 'react-image-file-resizer';
import axios from 'axios';
import { Avatar } from 'antd';

const ImageUpload = ({ ad, setAd }) => {
  const handleUpload = async (e) => {
    try {
      let files = e.target.files;
      files = [...files];

      if (files?.length) {
        console.log(files);
        setAd({ ...ad, uploading: true });
        files.forEach((file) => {
          new Promise(() => {
            Resizer.imageFileResizer(
              file,
              1080,
              720,
              'JPEG',
              100,
              0,
              async (uri) => {
                try {
                  const { data } = await axios.post('/upload-image', {
                    image: uri,
                  });
                  setAd((prev) => {
                    return {
                      ...prev,
                      photos: [data, ...prev.photos],
                      uploading: false,
                    };
                  });
                } catch (error) {
                  console.log(error);
                  setAd({ ...ad, uploading: false });
                }
              },
              'base64'
            );
          });
        });
      }
    } catch (error) {
      console.log(error);

      setAd({ ...ad, uploading: false });
    }
  };

  const handleDelete = async (file) => {
    const answer = window.confirm('Delete Image ?');

    if (!answer) return;
    setAd({ ...ad, uploading: true });

    try {
      const { data } = await axios.post('/remove-image', file);

      if (data?.ok) {
        setAd((prev) => {
          return {
            ...prev,
            photos: prev.photos.filter((p) => {
              return p.Key !== file.Key;
            }),
            uploading: false,
          };
        });
      }
    } catch (error) {
      console.log(error);
      setAd({ ...ad, uploading: false });
    }
  };

  return (
    <>
      <label className="btn btn-secondary mb-4">
        {ad.uploading ? 'Processing' : 'Upload Photos'}

        <input
          type="file"
          accept="image/*"
          multiple
          onChange={handleUpload}
          hidden
        />
      </label>
      {ad.photos?.map((file) => {
        return (
          <Avatar
            src={file?.Location}
            key={file.Key}
            shape="square"
            size="46"
            className="ml-2 mb-4"
            onClick={() => handleDelete(file)}
          />
        );
      })}
    </>
  );
};

export default ImageUpload;
