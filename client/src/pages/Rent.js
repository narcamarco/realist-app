import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useAuth } from '../context/auth';
import AdCard from '../components/cards/AdCard';
import SearchForm from '../components/forms/SearchForm';

const Rent = () => {
  const [auth, setAuth] = useAuth();
  const [ads, setAds] = useState([]);

  const fetchAds = async () => {
    try {
      const { data } = await axios.get('/ads-for-rent');
      setAds(data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchAds();
  }, []);

  return (
    <div>
      <SearchForm />
      <h1 className="display-1 bg-primary text-light p-5">For Rent</h1>
      <div className="container">
        <div className="row">
          {ads?.map((ad) => {
            return <AdCard ad={ad} key={ad._id} />;
          })}
        </div>
      </div>
    </div>
  );
};

export default Rent;
