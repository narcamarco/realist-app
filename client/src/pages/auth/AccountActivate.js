import React, { useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import axios from 'axios';
import toast from 'react-hot-toast';
import { useAuth } from '../../context/auth';

const AccountActivate = () => {
  const [auth, setAuth] = useAuth();
  const { token } = useParams();

  const navigate = useNavigate();
  const requestActivation = async () => {
    try {
      const { data } = await axios.post(`/register`, { token });

      if (data?.error) {
        toast.error(data.error);
      } else {
        localStorage.setItem('auth', JSON.stringify(data));
        setAuth(data);
        toast.success('Successfully logged In. Welcome to the Realist App');
        navigate('/');
      }
    } catch (error) {
      console.log(error);
      toast.error('Something went wrong. Please try again');
    }
  };

  useEffect(() => {
    if (token) {
      requestActivation();
    }
  }, [token]);

  return (
    <div
      className="display-1 d-flex justify-content-center align-items-center vh-100"
      style={{ marginTop: '-5%' }}
    >
      Please wait ...
    </div>
  );
};

export default AccountActivate;
