import React, { useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import axios from 'axios';
import toast from 'react-hot-toast';
import { useAuth } from '../../context/auth';

const AccessAccount = () => {
  const [auth, setAuth] = useAuth();
  const { token } = useParams();

  const navigate = useNavigate();
  const requestAccess = async () => {
    try {
      const { data } = await axios.post(`/access-account`, {
        resetCode: token,
      });

      if (data?.error) {
        toast.error(data.error);
      } else {
        localStorage.setItem('auth', JSON.stringify(data));
        setAuth(data);
        toast.success('Please update your password in profile page');
        navigate('/');
      }
    } catch (error) {
      console.log(error);
      toast.error('Something went wrong. Please try again');
    }
  };

  useEffect(() => {
    if (token) {
      requestAccess();
    }
  }, [token]);

  return (
    <div
      className="display-1 d-flex justify-content-center align-items-center vh-100"
      style={{ marginTop: '-5%' }}
    >
      Please wait ...
    </div>
  );
};

export default AccessAccount;
