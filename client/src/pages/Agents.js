import React, { useEffect, useState } from 'react';
import axios from 'axios';
import UserCard from '../components/cards/UserCard';

const Agents = () => {
  const [agents, setAgents] = useState();
  const [loading, setLoading] = useState(true);

  const fetchAgents = async () => {
    try {
      const { data } = await axios.get('/agents');
      setAgents(data);
      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchAgents();
  }, []);

  if (loading) {
    return (
      <div
        className="d-flex justify-content-center align-items-center vh-100"
        style={{ marginTop: '-10%' }}
      >
        <div className="display-1">Loading ...</div>
      </div>
    );
  }

  return (
    <div>
      <h1 className="display-1 bg-primary text-light p-5">Agents</h1>
      <div className="container">
        <div className="row">
          {agents?.map((agent) => {
            return <UserCard user={agent} key={agent._id} />;
          })}
        </div>
      </div>
    </div>
  );
};

export default Agents;
