import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useAuth } from '../context/auth';
import AdCard from '../components/cards/AdCard';
import SearchForm from '../components/forms/SearchForm';

const Home = () => {
  const [auth, setAuth] = useAuth();

  const [adsForSell, setAdsForSell] = useState();
  const [adsForRent, setAdsForRent] = useState();

  const fetchAds = async () => {
    try {
      const { data } = await axios.get('/ads');

      setAdsForSell(data.adsForSell);
      setAdsForRent(data.adsForRent);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchAds();
  }, []);

  return (
    <div>
      <SearchForm />
      <h1 className="display-1 bg-primary text-light p-5">For Sell</h1>
      <div className="container">
        <div className="row">
          {adsForSell?.map((ad) => {
            return <AdCard ad={ad} key={ad._id} />;
          })}
        </div>
      </div>

      <h1 className="display-1 bg-primary text-light p-5">For Rent</h1>
      <div className="container">
        <div className="row">
          {adsForRent?.map((ad) => {
            return <AdCard ad={ad} key={ad._id} />;
          })}
        </div>
      </div>
    </div>
  );
};

export default Home;
