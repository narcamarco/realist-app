import axios from 'axios';
import React, { useState } from 'react';
import Sidebar from '../../components/nav/Sidebar';
import toast from 'react-hot-toast';

const Setting = () => {
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      setLoading(true);
      const { data } = await axios.put('/update-password', {
        password,
      });

      if (data?.error) {
        toast.error(data.error);
        setLoading(false);
      } else {
        setLoading(false);
        toast.success('Password Updated');
      }
    } catch (error) {
      console.log(error);
      setLoading(false);
      toast.error(error);
    }
  };

  return (
    <>
      <h1 className="display-1 bg-primary text-light p-5">Setting</h1>
      <div className="container-fluid">
        <Sidebar />
        <div className="container mt-2">
          <div className="row">
            <div className="col-lg-8 offset-lg-2 mt-2">
              <form onSubmit={handleSubmit}>
                <input
                  type="password"
                  placeholder="Enter your password"
                  className="form-control mb-4"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />

                <button
                  type="submit"
                  className="btn btn-primary col-12 mb-4"
                  disabled={loading}
                >
                  {loading ? 'Processing' : 'Update Password'}
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Setting;
