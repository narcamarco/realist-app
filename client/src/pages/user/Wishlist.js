import axios from 'axios';
import React, { useState, useEffect } from 'react';
import AdCard from '../../components/cards/AdCard';
import Sidebar from '../../components/nav/Sidebar';
import { useAuth } from '../../context/auth';

const Wishlist = () => {
  const [auth, setAuth] = useAuth();

  const [ads, setAds] = useState([]);
  const [loading, setLoading] = useState(false);

  const isAuthenticated = auth.token !== '';
  const fetchAds = async () => {
    try {
      setLoading(true);
      const { data } = await axios.get(`/wishlist`);

      setAds(data);
      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchAds();
  }, [isAuthenticated]);

  return (
    <div>
      <h1 className="display-1 bg-primary text-light p-5">Wishlist</h1>
      <Sidebar />

      {!ads?.length ? (
        <div
          className="d-flex justify-content-center align-items-center vh-100"
          style={{ marginTop: '-10%' }}
        >
          <h2 className="">
            Hey {auth.user?.name ? auth.user?.name : auth.user?.username}, You
            have not liked any properties yet
          </h2>
        </div>
      ) : (
        <div className="container">
          <div className="row">
            <div className="col-lg-8 offset-lg-2 mt-4 mb-4">
              <p className="text-center">
                You have liked {ads?.length} properties
              </p>
            </div>
          </div>

          <div className="row">
            {ads?.map((ad, index) => {
              return <AdCard ad={ad} key={index} />;
            })}
          </div>
        </div>
      )}
    </div>
  );
};

export default Wishlist;
