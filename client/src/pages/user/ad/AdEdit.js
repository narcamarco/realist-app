import React, { useState, useEffect } from 'react';
import GooglePlacesAutocomplete from 'react-google-places-autocomplete';
import CurrencyInput from 'react-currency-input-field';
import axios from 'axios';
import { useNavigate, useParams } from 'react-router-dom';
import toast from 'react-hot-toast';
import { GOOGLE_PLACES_KEY } from '../../../config';
import ImageUpload from '../../../components/forms/ImageUpload';
import Sidebar from '../../../components/nav/Sidebar';

const AdEdit = ({ action, type }) => {
  const [ad, setAd] = useState({
    _id: '',
    photos: [],
    uploading: false,
    price: '',
    address: '',
    bedrooms: '',
    bathrooms: '',
    carpark: '',
    landsize: '',
    title: '',
    description: '',
    loading: false,
    type,
    action,
  });

  const [loaded, setLoaded] = useState(false);
  const navigate = useNavigate();
  const params = useParams();

  const handleClick = async () => {
    try {
      if (!ad.photos.length) {
        toast.error('Photo is required');
        return;
      } else if (!ad.price) {
        toast.error('Price is required');
        return;
      } else if (!ad.description) {
        toast.error('Description is required');
        return;
      } else {
        setAd({ ...ad, loading: true });
        const { data } = await axios.put(`/estate/${ad._id}`, ad);

        if (data?.error) {
          toast.error(data.error);
          setAd({ ...ad, loading: false });
        } else {
          toast.success('Ad Updated Successfully');

          setAd({ ...ad, loading: false });
          navigate('/dashboard');
        }
      }
    } catch (error) {
      console.log(error);
      setAd({ ...ad, loading: false });
    }
  };

  const handleDelete = async () => {
    try {
      setAd({ ...ad, loading: true });
      const { data } = await axios.delete(`/estate/${ad._id}`);

      if (data?.error) {
        toast.error(data.error);
        setAd({ ...ad, loading: false });
      } else {
        toast.success('Ad deleted Successfully');

        setAd({ ...ad, loading: false });
        navigate('/dashboard');
      }
    } catch (error) {
      console.log(error);
      setAd({ ...ad, loading: false });
    }
  };

  const fetchAd = async () => {
    try {
      const { data } = await axios.get(`/estate/${params.slug}`);
      setAd(data.ad);
      setLoaded(true);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (params?.slug) {
      fetchAd();
    }
  }, [params?.slug]);

  return (
    <div>
      <h1 className="display-1 bg-primary text-light p-5">Ad Edit</h1>
      <Sidebar />
      <div className="container">
        <div className="mb-3 form-control">
          <ImageUpload ad={ad} setAd={setAd} />
          {loaded ? (
            <GooglePlacesAutocomplete
              apiKey={GOOGLE_PLACES_KEY}
              apiOptions="au"
              selectProps={{
                defaultInputValue: ad?.address,
                placeholder: 'Search for address...',
                onChange: ({ value }) => {
                  setAd({ ...ad, address: value.description });
                },
              }}
            />
          ) : null}
        </div>

        {loaded ? (
          <div style={{ marginTop: '80px' }}>
            <CurrencyInput
              placeholder="Enter price"
              defaultValue={ad.price}
              className="form-control mb-3"
              onValueChange={(value) => {
                setAd({ ...ad, price: value });
              }}
            />
          </div>
        ) : null}

        {ad.type === 'House' && (
          <>
            <input
              type="number"
              min="0"
              className="form-control mb-3 "
              placeholder="Enter how many bedrooms"
              value={ad.bedrooms}
              onChange={(e) => {
                setAd({ ...ad, bedrooms: e.target.value });
              }}
            />

            <input
              type="number"
              min="0"
              className="form-control mb-3 "
              placeholder="Enter how many bathrooms"
              value={ad.bathrooms}
              onChange={(e) => {
                setAd({ ...ad, bathrooms: e.target.value });
              }}
            />

            <input
              type="number"
              min="0"
              className="form-control mb-3 "
              placeholder="Enter how many carpark"
              value={ad.carpark}
              onChange={(e) => {
                setAd({ ...ad, carpark: e.target.value });
              }}
            />
          </>
        )}

        <input
          type="text"
          className="form-control mb-3 "
          placeholder="Enter Size of land"
          value={ad.landsize}
          onChange={(e) => {
            setAd({ ...ad, landSize: e.target.value });
          }}
        />

        <input
          type="text"
          className="form-control mb-3 "
          placeholder="Enter title"
          value={ad.title}
          onChange={(e) => {
            setAd({ ...ad, title: e.target.value });
          }}
        />

        <textarea
          className="form-control mb-3 "
          placeholder="Enter Description"
          value={ad.description}
          onChange={(e) => {
            setAd({ ...ad, description: e.target.value });
          }}
        />

        <div className="d-flex justify-content-between">
          <button
            className={`btn btn-primary ${ad.loading ? 'disabled' : ''} mb-5`}
            onClick={handleClick}
          >
            {ad.loading ? 'Saving' : 'Submit'}
          </button>

          <button
            className={`btn btn-danger ${ad.loading ? 'disabled' : ''} mb-5`}
            onClick={handleDelete}
          >
            {ad.loading ? 'Deleting...' : 'Delete'}
          </button>
        </div>
      </div>
    </div>
  );
};

export default AdEdit;
