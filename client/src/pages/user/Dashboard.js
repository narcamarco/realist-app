import axios from 'axios';
import React, { useState, useEffect } from 'react';
import UserAdCard from '../../components/cards/UserAdCard';
import Sidebar from '../../components/nav/Sidebar';
import { useAuth } from '../../context/auth';

const Dashboard = () => {
  const [auth, setAuth] = useAuth();

  const [ads, setAds] = useState([]);
  const [total, setTotal] = useState(0);
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);

  const seller = auth.user?.role?.includes('Seller');

  const isAuthenticated = auth.token !== '';
  const fetchAds = async () => {
    try {
      setLoading(true);
      const { data } = await axios.get(`/user-ads/${page}`);
      setAds([...ads, ...data.ads]);
      setTotal(data.total);
      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchAds();
  }, [isAuthenticated]);

  useEffect(() => {
    if (page === 1) return;

    fetchAds();
  }, [page]);

  return (
    <div>
      <h1 className="display-1 bg-primary text-light p-5">Dashboard</h1>
      <Sidebar />

      {!seller ? (
        <div
          className="d-flex justify-content-center align-items-center vh-100"
          style={{ marginTop: '-10%' }}
        >
          <h2 className="">
            Hey {auth.user?.name ? auth.user?.name : auth.user?.username},
            Welcome to the Realist App
          </h2>
        </div>
      ) : (
        <div className="container">
          <div className="row">
            <div className="col-lg-8 offset-lg-2 mt-4 mb-4">
              <p className="text-center">Total {total} ads found</p>
            </div>
          </div>

          <div className="row">
            {ads?.map((ad, index) => {
              return <UserAdCard ad={ad} key={index} />;
            })}
          </div>

          {ads?.length < total ? (
            <div className="row">
              <div className="col text-center mt-4 mb-4">
                <button
                  disabled={loading}
                  className="btn btn-warning"
                  onClick={(e) => {
                    e.preventDefault();
                    setPage((prev) => {
                      return prev + 1;
                    });
                  }}
                >
                  {loading
                    ? 'Loading...'
                    : `${ads?.length} / ${total} Load More`}
                </button>
              </div>
            </div>
          ) : null}
        </div>
      )}
    </div>
  );
};

export default Dashboard;
